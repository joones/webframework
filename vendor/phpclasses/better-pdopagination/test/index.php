<meta charset="utf-8" />
<?php
/**
 * This is a simples test file
 * Be creative to implement to your needs!
 *
 * @category   Database Pagination Test
 * @package    PDO_Pagination
 * @author     Gilberto Albino <www@gilbertoalbino.com>
 * @copyright  2010-2012 Gilberto Albino
 * @license    Not applied
 * @version    Release: 2.0
 * @since      Class available since Release 1.0
 */

/** Check your file system path! **/
require_once dirname( __FILE__ ) . '/../libs/PDO_Pagination/PDO_Pagination.php';

/**
 * The reason why I kept this conection variable outside the class
 * is because you may need to inject multiple connections in the PDO_Pagination
 * So it gets pretty easier for you 
 * Feel free to use any connection logic you may wish!
 */
try {
    /**
     * The PDO String connection is for mysql
     * Check the documentation for your database
     */
    $connection = new PDO(
        'mysql:host=localhost;dbname=test', 
        'root', 
        ''
    );  
    /** As I am brazilian I always use UTF-8 enconding 
     * in files, connections and charset 
     */
    $connection->query("SET NAMES UTF8");
    
} catch ( PDOException $e ) {
    
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
    
}

/** 
 * Here we make an object from PDO_Pagination 
 * injecting the $connection variable that is an instance of PDO
 **/
$pagination = new PDO_Pagination( $connection );

/**
 * Once you have created an PDO_Pagination object you can
 * set the following parameters:
 * 
 * @example
 *   $pagination->setLimitPerPage( $limit );
 *   $pagination->setPaginator( $paginator );
 *   $pagination->setRange( $range );
 *   $pagination->setSQL( $string );
 *  
 */



/** Duh, the SQL itself **/
$pagination->setSQL( "SELECT * FROM colors ORDER BY color_hex" );
$pagination->setPaginator( 'paghex' );

$results = $connection->query( $pagination->getSQL() );

/**
 * Basic HTML for testing purpuses. No styles in the package 
 * Feel free style it as you wish the following selectors
 * #result-bar {} 
 * #result-bar span {}
 * #result-bar-not-found {}
 * #navigation-bar {}
 * #navigation-bar a span {}
 * #navigation-bar a.first {}
 * #navigation-bar a.previous {} 
 * #navigation-bar a.current {}
 * #navigation-bar a.others {}
 * #navigation-bar a.next {}
 * #navigation-bar a.last {}  
 */
print '<div style="float:left; margin-right: 25px">';
print '<h1>Ordered by Hexadecimal Value</h1>';

/**
 * The result bar will inform you if no results were found
 * so you should keep it outside the controll structure below 
 */
$pagination->printResultBar();

if( $pagination->getTotalOfResults() > 0    ) {
    print '<table width="500" cellpadding="4" border="1">';
    print '<tr><td></td><th>Hexadecimal</th><th>Name</th></tr>';
    foreach( $results as $r ) {
        printf("
            <tr>
                <td width=\"20\" style=\"background: %s\"></td>
                <td width=\"70\">%s</td>
                <td>%s</td>        
            </tr>
            "
            , $r['color_hex']
            , $r['color_hex']
            , $r['color_name']
        );
    }
    print '</table>';

    $pagination->printNavigationBar();
}
print '</div>';


/**
 * Here you could even be connected from another database
 * $connection2 = new PDO( database two connection string )
 */
$pagination->setSQL( "SELECT * FROM colors ORDER BY color_name" );
$pagination->setPaginator( 'pagname' );

$results = $connection->query( $pagination->getSQL() );

print '<div style="float:left">';
print '<h1>Order by Color Name</h1>';

/**
 * The result bar will inform you if no results were found
 * so you should keep it outside the controll structure below 
 */
$pagination->printResultBar();

if( $pagination->getTotalOfResults() > 0    ) {
    print '<table width="500" cellpadding="4" border="1">';
    print '<tr><td></td><th>Hexadecimal</th><th>Name</th></tr>';
    foreach( $results as $r ) {
        printf("
            <tr>
                <td width=\"20\" style=\"background: %s\"></td>
                <td width=\"70\">%s</td>
                <td>%s</td>        
            </tr>
            "
            , $r['color_hex']
            , $r['color_hex']
            , $r['color_name']
        );
    }
    print '</table>';

    $pagination->printNavigationBar();
}
print '</div>';

/** Just for the sake of good habit **/
$connection = null; 