<?php

namespace Core\Adapter;

use Core\Di\Container;

class AuthAdpter
{
    public function __construct()
    {
        //session_start();
    }

    public function getIdentity()
    {
        $userSession = $this->checkUserSession();
        return $userSession;
    }

    private function checkUserSession()
    {
        session_start();
        if (isset($_SESSION['EMAIL']) && isset($_SESSION['PASS'])) {
            $email = $_SESSION['EMAIL'];
            $pass  = $_SESSION['PASS'];
        } else {
            $email = "";
            $pass  = "";
        }

        $user      = Container::getClass("User", 'admin');
        $validated = $user->findByCrenditials($email, $pass);

        return $validated;
    }
}