<?php

namespace Core\Controller;

class ActionController
{
    protected $action;
    protected $namespace;
    protected $controller;
    protected $view;

    public function __construct()
    {
        $this->view = new \stdClass();
    }

    public function render($action, $layout = true)
    {
        $this->action = $action;

        $current = get_class($this);
        $exp_current_class = explode("\\", $current, 3);

        $this->namespace = $exp_current_class[0];
        $this->controller = strtolower($exp_current_class[2]);

        if ($layout == true && file_exists("../" . $this->namespace . "/view/layout/layout.phtml")) {
            include_once ("../" . $this->namespace . "/view/layout/layout.phtml");
        } else {
            $this->content();
        }
    }

    public function content()
    {
        $class_name = str_replace("controller", "", $this->controller);
        include_once ('../' . $this->namespace . '/view/' . $class_name . '/' . $this->action . '.phtml');
    }

    public static function redirect($module, $msgCallback)
    {
        $callback = "";

        if (!empty($msgCallback)) {
            $callback .= "?msg=" . $msgCallback;
        }

        return header("Location: " . baseUrl . $module . $callback);
    }
}