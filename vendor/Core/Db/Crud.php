<?php

namespace Core\Db;

class Crud extends InitDb
{
    public function create($fields, $values)
    {
        try {
            $f = implode(",", $fields);
            $v = implode("','", $values);
            $v = "'" . $v . "'";

            $query = "INSERT INTO {$this->table} ({$f}) VALUES ({$v})";
            $stmt = $this->db->prepare($query);
            return $stmt->execute();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function update($data, $id)
    {
        try {
            $query = "UPDATE {$this->table} SET ";
            $totalItens = count($data);
            $counter = 0;

            foreach ($data as $label => $val) {
                $counter++;
                $comma = ($counter < $totalItens) ? ", " : "";
                $key = $label . "=:" . $label . $comma;
                $query .= $key;
            }

            $query .= " WHERE id=:id";
            $stmt = $this->db->prepare($query);

            foreach ($data as $column => $value) {
                $stmt->bindValue(":" . $column, $value);
            }
            $stmt->bindValue(":id", $id);
            $update = $stmt->execute();

            return $update;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function delete($id)
    {
        try {
            $query = "UPDATE {$this->table} 
                        SET deleted=:deleted, updated_at=:updated_at
                        WHERE id=:id";

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(":deleted", 1);
            $stmt->bindValue(":updated_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":id", $id);
            return $stmt->execute();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}