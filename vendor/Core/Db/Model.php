<?php

namespace Core\Db;

class Model extends InitDb
{
    public function find($id)
    {
        $query = "SELECT * FROM {$this->table} WHERE id={$id}";
        $stmt = $this->db->query($query);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function findAll()
    {
        $query = "SELECT * FROM {$this->table} WHERE deleted='0'";
        $stmt = $this->db->query($query);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getTable()
    {
        return $this->table;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }
}
