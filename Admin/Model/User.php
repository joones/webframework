<?php

namespace Admin\Model;

use Core\Db\Model;

class User extends Model
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db);

        $this->table = 'user';
    }

    public function findByCrenditials($email, $pass)
    {
        try {
            $query = "SELECT name, email, password FROM user 
                        WHERE email=:email AND password=:password AND status=:status";
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(":email", $email);
            $stmt->bindValue(":password", $pass);
            $stmt->bindValue(":status", "A");
            $stmt->execute();

            if ($stmt->rowCount() == 1) {
                return $stmt->fetch();
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}