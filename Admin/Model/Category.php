<?php

namespace Admin\Model;

use Core\Db\Model;

class Category extends Model
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db);

        $this->table = 'category';
    }
}