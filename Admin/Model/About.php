<?php

namespace Admin\Model;

use Core\Db\Model;

class About extends Model
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db);

        $this->table = 'about';
    }
}