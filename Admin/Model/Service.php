<?php

namespace Admin\Model;

use Core\Db\Model;

class Service extends Model
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db);

        $this->table = 'service';
    }
}