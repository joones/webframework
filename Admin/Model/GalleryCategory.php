<?php

namespace Admin\Model;

use Core\Db\Model;

class GalleryCategory extends Model
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db);

        $this->table = 'gallery_category';
    }
}