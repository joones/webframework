<?php

namespace Admin\Model;

use Core\Db\Model;

class PortfolioCategory extends Model
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db);

        $this->table = 'portfolio_category';
    }
}