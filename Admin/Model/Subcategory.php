<?php

namespace Admin\Model;

use Core\Db\Model;

class Subcategory extends Model
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db);

        $this->table = 'subcategory';
    }

    public function findByCategory($id)
    {
        try {
            $query = "SELECT id, category_id, name FROM subcategory 
                        WHERE category_id=:category_id AND deleted=:deleted AND status=:status";
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(":category_id", $id);
            $stmt->bindValue(":deleted", 0);
            $stmt->bindValue(":status", "A");
            $stmt->execute();

            if ($stmt->rowCount() == 1) {
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}