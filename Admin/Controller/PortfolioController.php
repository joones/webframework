<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;
use Core\Pagination\Paginator;

class PortfolioController extends ActionController
{
    public function indexAction()
    {
        return $this->render('index');
    }

    public function gridAction()
    {
        $query = "SELECT p.id, p.name, p.link, p.status, c.name as category 
                  FROM portfolio AS p
                  INNER JOIN portfolio_category as c
                  ON c.id = p.portfolio_category_id
                  WHERE p.deleted = '0' ORDER BY p.id DESC";
        $pagination = new Paginator(Bootstrap::getDb());
        $pagination->setSQL($query);
        $pagination->setPaginator('page');
        $results = Bootstrap::getDb()->query($pagination->getSQL());
        $this->view->paginator = $pagination;
        $this->view->data = $results;

        return $this->render('grid', false);
    }

    public function newAction()
    {
        $this->view->categories = self::listCategories();
        $this->render('new', false);
    }

    public function editAction()
    {
        $model = Container::getClass("Portfolio", 'admin');
        $data  = $model->find($_GET['id']);
        $this->view->data = $data;
        $this->view->categories = self::listCategories();

        $this->render('edit', false);
    }

    public function persistAction()
    {
        $currentDate  = date("Y-m-d H:i:s");
        $image_name  = $_FILES["image"]["name"];

        if ($image_name != null) {
            $tmp_name  =  $_FILES["image"]["tmp_name"];
            $dir = "../public/uploads/portfolio/" . $image_name;

            if (!move_uploaded_file($tmp_name, $dir)) {
                return self::redirect('admin/portfolio/', 'error');
            }
        }

        $fields = [
            'portfolio_category_id', 'name', 'image', 'link', 'status', 'created_at', 'updated_at', 'deleted'
        ];

        $values = [
            $_POST['category'], $_POST['name'], $image_name, $_POST['link'], $_POST['status'], $currentDate, $currentDate, 0
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('portfolio');

        if ($crud->create($fields, $values)) {
            return self::redirect('admin/portfolio/', 'success');
        } else {
            return self::redirect('admin/portfolio/', 'error');
        }
    }

    public function updateAction()
    {
        $id = (int)$_POST['id'];
        $currentDate = date("Y-m-d H:i:s");
        $image_name  = $_FILES["image"]["name"];

        $model = Container::getClass("Portfolio", 'admin');
        $portfolio  = $model->find($id);

        $data = [
            'portfolio_category_id' => $_POST['category'],
            'name' => $_POST['name'],
            'link' => $_POST['link'],
            'status' => $_POST['status'],
            'updated_at' => $currentDate
        ];

        if ($image_name != null) {
            $tmp_name  =  $_FILES["image"]["tmp_name"];
            $dir = "../public/uploads/portfolio/" . $image_name;

            if (move_uploaded_file($tmp_name, $dir)) {
                $data['image'] = $image_name;
            } else {
                return self::redirect('admin/portfolio/', 'error');

            }
        }

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('portfolio');

        if ($crud->update($data, $id)) {
            return self::redirect('admin/portfolio/', 'success');
        } else {
            return self::redirect('admin/portfolio/', 'error');
        }
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'deleted' => 1,
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('portfolio');

        return $crud->update($data, $id);
    }

    private static function listCategories()
    {
        $model = Container::getClass("PortfolioCategory", 'admin');
        return $model->findAll();
    }
}














