<?php

namespace Admin\Controller;

use Core\Controller\ActionController;

class BlogController extends ActionController
{
    public function indexAction()
    {
        $this->render('index');
    }
}