<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;
use Core\Pagination\Paginator;

class ServiceController extends ActionController
{
    public function indexAction()
    {
        return $this->render('index');
    }

    public function gridAction()
    {
        $pagination = new Paginator(Bootstrap::getDb());
        $pagination->setSQL("SELECT id, title, status FROM service WHERE deleted = '0' ORDER BY id");
        $pagination->setPaginator('page');
        $results = Bootstrap::getDb()->query($pagination->getSQL());
        $this->view->paginator = $pagination;
        $this->view->data = $results;

        return $this->render('grid', false);
    }

    public function newAction()
    {
        $this->render('new', false);
    }

    public function editAction()
    {
        $model = Container::getClass("Service", 'admin');
        $data  = $model->find($_GET['id']);
        $this->view->data = $data;

        $this->render('edit', false);
    }

    public function persistAction()
    {
        $currentDate = date("Y-m-d H:i:s");
        $image_name  = $_FILES["image"]["name"];

        if ($image_name != null) {
            $tmp_name  =  $_FILES["image"]["tmp_name"];
            $dir = "../public/uploads/service/" . $image_name;

            if (!move_uploaded_file($tmp_name, $dir)) {
                return self::redirect('admin/service/', 'error');
            }
        }

        $fields = [
            'title', 'description', 'image', 'status', 'created_at', 'updated_at', 'deleted'
        ];

        $values = [
            $_POST['title'], $_POST['description'], $image_name, $_POST['status'], $currentDate, $currentDate, 0
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('service');

        if ($crud->create($fields, $values)) {
            return self::redirect('admin/service/', 'success');
        } else {
            return self::redirect('admin/service/', 'error');
        }
    }

    public function updateAction()
    {
        $id = (int)$_POST['id'];
        $currentDate = date("Y-m-d H:i:s");
        $image_name  = $_FILES["image"]["name"];

        $data = [
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'status' => $_POST['status'],
            'updated_at' => $currentDate
        ];

        if ($image_name != null) {
            $tmp_name  =  $_FILES["image"]["tmp_name"];
            $dir = "../public/uploads/service/" . $image_name;

            if (move_uploaded_file($tmp_name, $dir)) {
                $data['image'] = $image_name;
            } else {
                return self::redirect('admin/service/', 'error');

            }
        }

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('service');

        if ($crud->update($data, $id)) {
            return self::redirect('admin/service/', 'success');
        } else {
            return self::redirect('admin/service/', 'error');
        }
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'deleted' => 1,
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('service');

        return $crud->update($data, $id);
    }
}