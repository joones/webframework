<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;
use Core\Pagination\Paginator;

class UserController extends ActionController
{
    public function indexAction()
    {
        $this->render('index');
    }

    public function gridAction()
    {
        $pagination = new Paginator(Bootstrap::getDb());
        $pagination->setSQL("SELECT id, name, email, status FROM user WHERE deleted = '0' ORDER BY id");
        $pagination->setPaginator('page');
        $results = Bootstrap::getDb()->query($pagination->getSQL());
        $this->view->paginator = $pagination;
        $this->view->user = $results;

        return $this->render('grid', false);
    }

    public function newAction()
    {
        $this->render('new', false);
    }

    public function editAction()
    {
        $model = Container::getClass("User", 'admin');
        $user  = $model->find($_GET['id']);
        $this->view->user = $user;

        $this->render('edit', false);
    }

    public function persistAction()
    {
        $currentDate = date("Y-m-d H:i:s");

        $password = md5($_GET['password']);
        $confirm  = md5($_GET['confirm']);

        if ($password == $confirm) {
            $fields = [
                'name', 'email', 'password', 'status', 'created_at', 'updated_at'
            ];

            $values = [
                $_GET['name'], $_GET['email'], md5($_GET['password']), $_GET['status'], $currentDate, $currentDate
            ];

            $crud = new Crud(Bootstrap::getDb());
            $crud->setTable('user');

            if ($crud->create($fields, $values)) {
                $this->view->success = true;
            } else {
                $this->view->error = true;
            }
        } else {
            $this->view->error = true;
        }

        return $this->render('persist', false);
    }

    public function updateAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'name' => $_GET['name'],
            'email' => $_GET['email'],
            'status' => $_GET['status'],
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('user');

        if ($crud->update($data, $id)) {
            $this->view->success = true;
        } else {
            $this->view->error = true;
        }

        $this->render('update', false);
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'deleted' => 1,
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('user');

        return $crud->update($data, $id);
    }

    public function changePasswordAction()
    {
        $model = Container::getClass("User", 'admin');
        $user  = $model->find($_GET['id']);
        $this->view->user = $user;

        $this->render('change-password', false);
    }

    public function updatePasswordAction()
    {
        $id    = (int)$_GET['id'];

        $model = Container::getClass("User", 'admin');
        $user  = $model->find($id);

        $current  = md5($_GET['current']);
        $password = md5($_GET['password']);
        $confirm  = md5($_GET['confirm']);


        if ($password == $confirm && $current == $user['password']) {
            $data = [
                'password' => md5($_GET['password']),
                'updated_at' => '2014-06-11 14:00:00'
            ];

            $crud = new Crud(Bootstrap::getDb());
            $crud->setTable('user');

            if ($crud->update($data, $id)) {
                $this->view->success = true;
            } else {
                $this->view->error = true;
            }
        } else {
            $this->view->error = true;
        }

        return $this->render('update-password', false);
    }
}










