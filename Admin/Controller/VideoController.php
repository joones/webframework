<?php

namespace Admin\Controller;

use Core\Controller\ActionController;

class VideoController extends ActionController
{
    public function indexAction()
    {
        $this->render('index');
    }
}