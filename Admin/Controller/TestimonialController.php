<?php

namespace Admin\Controller;

use Core\Controller\ActionController;

class TestimonialController extends ActionController
{
    public function indexAction()
    {
        $this->render('index');
    }
}