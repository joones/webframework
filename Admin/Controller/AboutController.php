<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;

class AboutController extends ActionController
{
    public function indexAction()
    {
        $model = Container::getClass("About", 'admin');
        $data  = $model->find(1);
        $this->view->data = $data;

        $this->render('index');
    }

    public function updateAction()
    {
        $currentDate = date("Y-m-d H:i:s");
        $image_name  = $_FILES["image"]["name"];

        $data = [
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'updated_at' => $currentDate
        ];

        if ($image_name != null) {
            $tmp_name  =  $_FILES["image"]["tmp_name"];
            $dir = "../public/uploads/about/" . $image_name;

            if (move_uploaded_file($tmp_name, $dir)) {
                $data['image'] = $image_name;
            } else {
                return self::redirect('admin/about/', 'error');

            }
        }

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('about');

        if ($crud->update($data, 1)) {
            return self::redirect('admin/about/', 'success');
        } else {
            return self::redirect('admin/about/', 'error');
        }
    }
}