<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;
use Core\Pagination\Paginator;

class GalleryController extends ActionController
{
    public function indexAction()
    {
        return $this->render('index');
    }

    public function gridAction()
    {
        $query = "SELECT g.id, g.name, g.status, c.name as category FROM gallery AS g
                  INNER JOIN gallery_category as c
                  ON c.id = g.gallery_category_id
                  WHERE g.deleted = '0' ORDER BY g.id DESC";
        $pagination = new Paginator(Bootstrap::getDb());
        $pagination->setSQL($query);
        $pagination->setPaginator('page');
        $results = Bootstrap::getDb()->query($pagination->getSQL());
        $this->view->paginator = $pagination;
        $this->view->data = $results;

        return $this->render('grid', false);
    }

    public function newAction()
    {
        $this->view->categories = self::listCategories();
        $this->render('new', false);
    }

    public function editAction()
    {
        $model = Container::getClass("Gallery", 'admin');
        $data  = $model->find($_GET['id']);
        $this->view->data = $data;
        $this->view->categories = self::listCategories();

        $this->render('edit', false);
    }

    public function persistAction()
    {
        $currentDate  = date("Y-m-d H:i:s");
        $products_dir = md5(time());
        mkdir('../public/uploads/gallery/' . $products_dir, 0777);

        $path    = '../public/uploads/gallery/' . $products_dir;
        $file    = $_FILES['images'];
        $numFile = count(array_filter($file['name']));

        if ($numFile > 0) {
            for ($i = 0; $i < $numFile; $i++) {
                $name = $file['name'][$i];
                $tmp  = $file['tmp_name'][$i];

                $extension = @end(explode('.', $name));
                $new_name = rand() . ".$extension";

                move_uploaded_file($tmp, $path . '/' . $new_name);
            }
        }

        $fields = [
            'gallery_category_id', 'name', 'gallery_dir', 'status', 'created_at', 'updated_at', 'deleted'
        ];

        $values = [
            $_POST['category'], $_POST['name'], $products_dir, $_POST['status'], $currentDate, $currentDate, 0
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('gallery');

        if ($crud->create($fields, $values)) {
            return self::redirect('admin/gallery/', 'success');
        } else {
            return self::redirect('admin/gallery/', 'error');
        }
    }

    public function updateAction()
    {
        $id = (int)$_POST['id'];
        $currentDate = date("Y-m-d H:i:s");

        $model = Container::getClass("Gallery", 'admin');
        $gallery  = $model->find($id);

        $path    = '../public/uploads/gallery/' . $gallery['gallery_dir'];
        $file    = $_FILES['images'];
        $numFile = count(array_filter($file['name']));

        if ($numFile > 0) {
            for ($i = 0; $i < $numFile; $i++) {
                $name = $file['name'][$i];
                $tmp  = $file['tmp_name'][$i];

                $extension = @end(explode('.', $name));
                $new_name = rand() . ".$extension";

                move_uploaded_file($tmp, $path . '/' . $new_name);
            }
        }

        $data = [
            'gallery_category_id' => $_POST['category'],
            'name' => $_POST['name'],
            'status' => $_POST['status'],
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('gallery');

        if ($crud->update($data, $id)) {
            return self::redirect('admin/gallery/', 'success');
        } else {
            return self::redirect('admin/gallery/', 'error');
        }
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'deleted' => 1,
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('gallery');

        return $crud->update($data, $id);
    }

    public function loadGalleryAction()
    {
        $model = Container::getClass("Gallery", 'admin');
        $gallery  = $model->find((int)$_GET['id']);
        $images = [];
        $path = '../public/uploads/gallery/' . $gallery['gallery_dir'] . '/';
        $op = opendir($path);
        $count = 0;
        while ($file = readdir($op)) {

            $ext = explode(".", $file);
            if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                $count++;
                $images[] = $count . "-" . $gallery['gallery_dir'] . '/' . $file;
            }
        }

        $this->view->images = $images;

        return $this->render('load-gallery', false);
    }

    public function deleteImageAction()
    {
        $model = Container::getClass("Gallery", 'admin');
        $gallery  = $model->find((int)$_GET['id']);

        $images = [];
        $path = '../public/uploads/gallery/' . $gallery['gallery_dir'] . '/';
        $op = opendir($path);
        $count = 0;
        while ($file = readdir($op)) {

            $ext = explode(".", $file);
            if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                $count++;
                if ($_GET['pos'] == $count) {
                    unlink($path . $file);
                    exit();
                }
            }
        }
    }

    private static function listCategories()
    {
        $model = Container::getClass("GalleryCategory", 'admin');
        return $model->findAll();
    }
}














