<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;
use Core\Pagination\Paginator;

class SubcategoryController extends ActionController
{
    public function indexAction()
    {
        return $this->render('index');
    }

    public function gridAction()
    {
        $query = "SELECT s.id, s.name, s.status, c.name as category 
                  FROM subcategory AS s
                  INNER JOIN category as c
                  ON c.id = s.category_id
                  WHERE s.deleted = '0' ORDER BY s.id DESC";
        $pagination = new Paginator(Bootstrap::getDb());
        $pagination->setSQL($query);
        $pagination->setPaginator('page');
        $results = Bootstrap::getDb()->query($pagination->getSQL());
        $this->view->paginator = $pagination;
        $this->view->data = $results;

        return $this->render('grid', false);
    }

    public function newAction()
    {
        $this->view->categories = self::listCategories();
        $this->render('new', false);
    }

    public function editAction()
    {
        $model = Container::getClass("Subcategory", 'admin');
        $data  = $model->find($_GET['id']);
        $this->view->data = $data;
        $this->view->categories = self::listCategories();

        $this->render('edit', false);
    }

    public function persistAction()
    {
        $currentDate = date("Y-m-d H:i:s");

        $fields = [
            'category_id', 'name', 'status', 'created_at', 'updated_at', 'deleted'
        ];

        $values = [
            $_GET['category'], $_GET['name'], $_GET['status'], $currentDate, $currentDate, 0
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('subcategory');

        if ($crud->create($fields, $values)) {
            $this->view->success = true;
        } else {
            $this->view->error = false;
        }

        $this->render('persist', false);
    }

    public function updateAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'category_id' => $_GET['category'],
            'name' => $_GET['name'],
            'status' => $_GET['status'],
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('subcategory');

        if ($crud->update($data, $id)) {
            $this->view->success = true;
        } else {
            $this->view->error = false;
        }

        $this->render('update', false);
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'deleted' => 1,
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('subcategory');

        return $crud->update($data, $id);
    }

    private static function listCategories()
    {
        $model = Container::getClass("Category", 'admin');
        return $model->findAll();
    }
}