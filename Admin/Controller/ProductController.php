<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;
use Core\Pagination\Paginator;

class ProductController extends ActionController
{
    public function indexAction()
    {
        return $this->render('index');
    }

    public function gridAction()
    {
        $query = "SELECT p.id, p.name, p.status, c.name as category 
                  FROM product AS p
                  INNER JOIN category as c
                  ON c.id = p.category_id
                  WHERE p.deleted = '0' ORDER BY p.id DESC";
        $pagination = new Paginator(Bootstrap::getDb());
        $pagination->setSQL($query);
        $pagination->setPaginator('page');
        $results = Bootstrap::getDb()->query($pagination->getSQL());
        $this->view->paginator = $pagination;
        $this->view->data = $results;

        return $this->render('grid', false);
    }

    public function newAction()
    {
        $this->view->categories = self::listCategories();
        $this->render('new', false);
    }

    public function editAction()
    {
        $model = Container::getClass("Product", 'admin');
        $data  = $model->find($_GET['id']);
        $this->view->data = $data;
        $this->view->categories = self::listCategories();

        $this->render('edit', false);
    }

    public function persistAction()
    {
        $currentDate = date("Y-m-d H:i:s");
        $products_dir = md5(time());
        mkdir('../public/uploads/product/' . $products_dir, 0777);

        $path    = '../public/uploads/product/' . $products_dir;

        $image_name  = $_FILES["image"]["name"];

        if ($image_name != null) {
            $tmp_name  =  $_FILES["image"]["tmp_name"];
            $dir = $path . "/" . $image_name;

            if (!move_uploaded_file($tmp_name, $dir)) {
                return self::redirect('admin/product/', 'error');
            }
        }

        $file    = $_FILES['images'];
        $numFile = count(array_filter($file['name']));

        if ($numFile > 0) {
            for ($i = 0; $i < $numFile; $i++) {
                $name = $file['name'][$i];
                $tmp  = $file['tmp_name'][$i];

                $extension = @end(explode('.', $name));
                $new_name = rand() . ".$extension";

                move_uploaded_file($tmp, $path . '/' . $new_name);
            }
        }

        $fields = [
            'category_id', 'subcategory_id', 'name', 'description', 'image', 'gallery_dir',
            'status', 'created_at', 'updated_at', 'deleted'
        ];

        $values = [
            $_POST['category'], $_POST['subcategory'], $_POST['name'], $_POST['description'], $image_name,
            $products_dir, $_POST['status'], $currentDate, $currentDate, 0
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('product');

        if ($crud->create($fields, $values)) {
            return self::redirect('admin/product/', 'success');
        } else {
            return self::redirect('admin/product/', 'error');
        }
    }

    public function updateAction()
    {
        $id = (int)$_POST['id'];
        $currentDate = date("Y-m-d H:i:s");
        $image_name  = $_FILES["image"]["name"];

        $data = [
            'category_id' => $_POST['category'],
            'subcategory_id' => $_POST['subcategory'],
            'name' => $_POST['name'],
            'description' => $_POST['description'],
            'status' => $_POST['status'],
            'updated_at' => $currentDate
        ];

        if ($image_name != null) {
            $tmp_name  =  $_FILES["image"]["tmp_name"];
            $dir = "../public/uploads/product/" . $image_name;

            if (move_uploaded_file($tmp_name, $dir)) {
                $data['image'] = $image_name;
            } else {
                return self::redirect('admin/product/', 'error');

            }
        }

        $model = Container::getClass("Product", 'admin');
        $product = $model->find($id);

        $path    = '../public/uploads/product/' . $product['gallery_dir'];
        $file    = $_FILES['images'];
        $numFile = count(array_filter($file['name']));

        if ($numFile > 0) {
            for ($i = 0; $i < $numFile; $i++) {
                $name = $file['name'][$i];
                $tmp  = $file['tmp_name'][$i];

                $extension = @end(explode('.', $name));
                $new_name = rand() . ".$extension";

                move_uploaded_file($tmp, $path . '/' . $new_name);
            }
        }

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('product');

        if ($crud->update($data, $id)) {
            return self::redirect('admin/product/', 'success');
        } else {
            return self::redirect('admin/product/', 'error');
        }
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'deleted' => 1,
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('product');

        return $crud->update($data, $id);
    }

    public function listSubcategoriesAction()
    {
        $model = Container::getClass("Subcategory", 'admin');
        $subcategories = $model->findByCategory((int)$_GET['category']);
        $this->view->subcategories = $subcategories;
        return $this->render('list-subcategories', false);
    }

    public function fetchSubcategoriesAction()
    {
        $model = Container::getClass("Subcategory", 'admin');
        $subcategories = $model->findByCategory((int)$_GET['category']);
        $this->view->subcategories = $subcategories;
        $this->view->subcategory_id = (int)$_GET['subcategory'];

        return $this->render('fetch-subcategories', false);
    }

    public function loadGalleryAction()
    {
        $model = Container::getClass("Product", 'admin');
        $product  = $model->find((int)$_GET['id']);
        $images = [];
        $path = '../public/uploads/product/' . $product['gallery_dir'] . '/';
        $op = opendir($path);
        $count = 0;
        while ($file = readdir($op)) {
            $ext = explode(".", $file);
            if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                $count++;
                $images[] = $count . "-" . $product['gallery_dir'] . '/' . $file;
            }
        }

        $this->view->images = $images;

        return $this->render('load-gallery', false);
    }

    public function deleteImageAction()
    {
        $model = Container::getClass("Product", 'admin');
        $product  = $model->find((int)$_GET['id']);

        $path = '../public/uploads/product/' . $product['gallery_dir'] . '/';
        $op = opendir($path);
        $count = 0;
        while ($file = readdir($op)) {

            $ext = explode(".", $file);
            if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                $count++;
                if ($_GET['pos'] == $count) {
                    unlink($path . $file);
                    exit();
                }
            }
        }
    }


    private static function listCategories()
    {
        $model = Container::getClass("Category", 'admin');
        return $model->findAll();
    }
}














