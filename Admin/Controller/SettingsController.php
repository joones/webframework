<?php

namespace Admin\Controller;

use Core\Controller\ActionController;

class SettingsController extends ActionController
{
    public function companySettingsAction()
    {
        $this->render('company-settings');
    }

    public function socialSettingsAction()
    {
        $this->render('social-settings');
    }
}