<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;

class LoginController extends ActionController
{
    public function indexAction()
    {
         $this->render('index', false);
    }

    public function authAction()
    {
        $email = $_GET['email'];
        $pass  = md5($_GET['pass']);

        $user        = Container::getClass("User", 'admin');
        $credentials = $user->findByCrenditials($email, $pass);

        if ($credentials) {
            $_SESSION['NAME']  = $credentials['name'];
            $_SESSION['EMAIL'] = $credentials['email'];
            $_SESSION['PASS']  = $credentials['password'];

            $this->view->error = false;
            $this->render('auth', false);
        } else {
            $this->view->error = true;
            $this->render('auth', false);
        }
    }

    public function logoutAction()
    {
        if (isset($_SESSION['EMAIL']) && isset($_SESSION['PASS'])) {
            unset($_SESSION['NAME']);
            unset($_SESSION['EMAIL']);
            unset($_SESSION['PASS']);

            return self::redirect('admin/', null);
        } else {
            return self::redirect('admin/', null);
        }
    }
}