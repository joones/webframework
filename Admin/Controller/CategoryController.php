<?php

namespace Admin\Controller;

use Core\Controller\ActionController;
use Core\Di\Container;
use Core\Db\Crud;
use Core\Init\Bootstrap;
use Core\Pagination\Paginator;

class CategoryController extends ActionController
{
    public function indexAction()
    {
        return $this->render('index');
    }

    public function gridAction()
    {
        $pagination = new Paginator(Bootstrap::getDb());
        $pagination->setSQL("SELECT id, name, status FROM category WHERE deleted = '0' ORDER BY id");
        $pagination->setPaginator('page');
        $results = Bootstrap::getDb()->query($pagination->getSQL());
        $this->view->paginator = $pagination;
        $this->view->data = $results;

        return $this->render('grid', false);
    }

    public function newAction()
    {
        $this->render('new', false);
    }

    public function editAction()
    {
        $model = Container::getClass("Category", 'admin');
        $data  = $model->find($_GET['id']);
        $this->view->data = $data;

        $this->render('edit', false);
    }

    public function persistAction()
    {
        $currentDate = date("Y-m-d H:i:s");

        $fields = [
            'name', 'status', 'created_at', 'updated_at', 'deleted'
        ];

        $values = [
            $_GET['name'], $_GET['status'], $currentDate, $currentDate, 0
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('category');

        if ($crud->create($fields, $values)) {
            $this->view->success = true;
        } else {
            $this->view->error = false;
        }

        $this->render('persist', false);
    }

    public function updateAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'name' => $_GET['name'],
            'status' => $_GET['status'],
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('category');

        if ($crud->update($data, $id)) {
            $this->view->success = true;
        } else {
            $this->view->error = false;
        }

        $this->render('update', false);
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $currentDate = date("Y-m-d H:i:s");

        $data = [
            'deleted' => 1,
            'updated_at' => $currentDate
        ];

        $crud = new Crud(Bootstrap::getDb());
        $crud->setTable('category');

        return $crud->update($data, $id);
    }
}