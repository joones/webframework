<?php
$router = [];
/*
 * Frontend routes
 */
$router['app'][] = ['namespace' => 'app', 'route' => '/', 'controller' => 'index', 'action' => 'index'];

/*
 * Backend routes
 */
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/', 'controller' => 'index', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/login/', 'controller' => 'login', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/auth/', 'controller' => 'login', 'action' => 'auth'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/logout/', 'controller' => 'login', 'action' => 'logout'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/settings/company-settings/', 'controller' => 'settings', 'action' => 'company-settings'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/settings/social-settings/', 'controller' => 'settings', 'action' => 'social-settings'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/', 'controller' => 'user', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/grid', 'controller' => 'user', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/new/', 'controller' => 'user', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/persist', 'controller' => 'user', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/edit', 'controller' => 'user', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/update', 'controller' => 'user', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/delete', 'controller' => 'user', 'action' => 'delete'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/change-password', 'controller' => 'user', 'action' => 'change-password'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/user/update-password', 'controller' => 'user', 'action' => 'update-password'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/banner/', 'controller' => 'banner', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/banner/grid', 'controller' => 'banner', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/banner/new/', 'controller' => 'banner', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/banner/persist', 'controller' => 'banner', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/banner/edit', 'controller' => 'banner', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/banner/update', 'controller' => 'banner', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/banner/delete', 'controller' => 'banner', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/blog/', 'controller' => 'blog', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/testimonial/', 'controller' => 'testimonial', 'action' => 'index'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/', 'controller' => 'gallery', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/grid', 'controller' => 'gallery', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/new/', 'controller' => 'gallery', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/persist', 'controller' => 'gallery', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/edit', 'controller' => 'gallery', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/update', 'controller' => 'gallery', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/delete', 'controller' => 'gallery', 'action' => 'delete'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/load-gallery', 'controller' => 'gallery', 'action' => 'load-gallery'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery/delete-image', 'controller' => 'gallery', 'action' => 'delete-image'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery-category/grid', 'controller' => 'gallery-category', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery-category/new/', 'controller' => 'gallery-category', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery-category/persist', 'controller' => 'gallery-category', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery-category/edit', 'controller' => 'gallery-category', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery-category/update', 'controller' => 'gallery-category', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/gallery-category/delete', 'controller' => 'gallery-category', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio/', 'controller' => 'portfolio', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio/grid', 'controller' => 'portfolio', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio/new/', 'controller' => 'portfolio', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio/persist', 'controller' => 'portfolio', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio/edit', 'controller' => 'portfolio', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio/update', 'controller' => 'portfolio', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio/delete', 'controller' => 'portfolio', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio-category/grid', 'controller' => 'portfolio-category', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio-category/new/', 'controller' => 'portfolio-category', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio-category/persist', 'controller' => 'portfolio-category', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio-category/edit', 'controller' => 'portfolio-category', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio-category/update', 'controller' => 'portfolio-category', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/portfolio-category/delete', 'controller' => 'portfolio-category', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/about/', 'controller' => 'about', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/about/update', 'controller' => 'about', 'action' => 'update'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/', 'controller' => 'product', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/grid', 'controller' => 'product', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/new/', 'controller' => 'product', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/persist', 'controller' => 'product', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/edit', 'controller' => 'product', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/update', 'controller' => 'product', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/delete', 'controller' => 'product', 'action' => 'delete'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/list-subcategories', 'controller' => 'product', 'action' => 'list-subcategories'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/fetch-subcategories', 'controller' => 'product', 'action' => 'fetch-subcategories'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/load-gallery', 'controller' => 'product', 'action' => 'load-gallery'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/product/delete-image', 'controller' => 'product', 'action' => 'delete-image'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/category/grid', 'controller' => 'category', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/category/new/', 'controller' => 'category', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/category/persist', 'controller' => 'category', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/category/edit', 'controller' => 'category', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/category/update', 'controller' => 'category', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/category/delete', 'controller' => 'category', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/partner/', 'controller' => 'partner', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/partner/grid', 'controller' => 'partner', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/partner/new/', 'controller' => 'partner', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/partner/persist', 'controller' => 'partner', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/partner/edit', 'controller' => 'partner', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/partner/update', 'controller' => 'partner', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/partner/delete', 'controller' => 'partner', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/customer/', 'controller' => 'customer', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/customer/grid', 'controller' => 'customer', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/customer/new/', 'controller' => 'customer', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/customer/persist', 'controller' => 'customer', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/customer/edit', 'controller' => 'customer', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/customer/update', 'controller' => 'customer', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/customer/delete', 'controller' => 'customer', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/subcategory/grid', 'controller' => 'subcategory', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/subcategory/new/', 'controller' => 'subcategory', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/subcategory/persist', 'controller' => 'subcategory', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/subcategory/edit', 'controller' => 'subcategory', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/subcategory/update', 'controller' => 'subcategory', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/subcategory/delete', 'controller' => 'subcategory', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/service/', 'controller' => 'service', 'action' => 'index'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/service/grid', 'controller' => 'service', 'action' => 'grid'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/service/new/', 'controller' => 'service', 'action' => 'new'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/service/persist', 'controller' => 'service', 'action' => 'persist'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/service/edit', 'controller' => 'service', 'action' => 'edit'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/service/update', 'controller' => 'service', 'action' => 'update'];
$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/service/delete', 'controller' => 'service', 'action' => 'delete'];

$router['admin'][] = ['namespace' => 'admin', 'route' => '/admin/video/', 'controller' => 'video', 'action' => 'index'];

$app = new Core\Init\Bootstrap($router, "/webframework");