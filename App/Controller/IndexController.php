<?php

namespace App\Controller;

use Core\Controller\ActionController;

class IndexController extends ActionController
{
    public function indexAction()
    {
        $this->render('index');
    }
}